## Links
* [https://github.com/CloudCoreo/t-private-network](https://github.com/CloudCoreo/t-private-network)
* [https://github.com/CloudCoreo/t-public-network](https://github.com/CloudCoreo/t-public-network)
* [https://github.com/CloudCoreo/t-demo-network](https://github.com/CloudCoreo/t-demo-network)
* [CloudCoreo Hub](http://hub.cloudcoreo.com/)
* [CloudCoreo.com](https://www.cloudcoreo.com/)
* [CloudCoreo CLI in NPMjs](https://www.npmjs.com/package/cloudcoreo-cli) and [on github](https://github.com/cloudcoreo/cloudcoreo-cli)
* [Conceptual docs](https://www.cloudcoreo.com/docs)
* [API docs](http://docs.cloudcoreo.com/docs/frames/index)

## Slide 1
CloudCoreo is a paradim shift in Cloud management software. A SaaS platform that aims to allow your cloud infrastructure to be treated like standard software from development to deployment. We have been referred to as many different things, including "a package manager for cloud" or off the record, "CloudFormation that doesn’t suck"

## Slide 2
Who are we?

First of all, we are a team of Cloud experts that includes myself and the actual founder of AWS EC2. I’ve been doing AWS work now for almost 8 years and was one of the first certified cloud architects in the world.

## Slide 3
So why do we exist? Because devops is broken, and that has lead to $60 B in downtime. 90% of all hacks and down time are from human error, configuration error, lack of best practices or missing updates.

## Slide 4
Leveraging the work of others is the only way we can all stay up to date. When I was consulting, I would often learn of new and/or better ways of doing things AFTER the contract was over. Customers had no way of knowing this, and I had no way of reasing it for them.

Whats more, if I wanted to deploy in Azure instaead of Amazon, I would have to start over.

## Slide 5
CloudCoreo is a repostiory of public stacks that can be searched. Think docker-hub or npmjs.org.
We are a SaaS platform that maintains stacks against a git repostiory continuously
We are an engine that can translate a stack to api calls that can be executed against any cloud provider.

## Slide 6
Since our beta release 11 months ago, we are now maintaing over 1100 stacks in AWS.

## Slide 7
Lets see it in action!

## Slide 8
Everything we do is open source except the engine itself. All of our stacks, CLI, even the docs. So please, if there is a better way, submit a pull request!

## Slide 9
CloudCoreo’s unit of packaging is the git repository.
Every repository is a stack
Every stack can extend or include an unlimited number of other stacks (repostories)
 
## Slide 10
The CloudCoreo CLI is written in Node and can be installed with NPM

```
$ npm install cloudcoreo-cli
```

 it is not required, but wraps most of the complex and annoying git commands
 
 Create a VPC

```
$ mkdir taos-demo
$ cd taos-demo
$ coreo init new-stack --stack-type stack
``` 
 
## Slide 11
Here is a standard block of CloudCoreo code – this in particular will ensure a VPC is created or find an existing one based on your specifications. One of the great things is that after you create a resource like this, you can refer to it by name everywhere else. We will see that later. Lets go create it now.


```
$ mkdir taos-demo
$ cd taos-demo
$ coreo init new-stack --stack-type stack
```
```ruby
## <repo>/services/config.rb
coreo_aws_vpc_vpc "taos-demo" do
  action :find_or_create
  cidr "33.33.0.0/16"
  internet_gateway true
end
```
Add some tags


## Slide 12

```ruby
## <repo>/services/config.rb
coreo_aws_vpc_routetable "taos-demo-pub-route" do
  action :sustain
  vpc "taos-demo"
  routes [
             { :from => "0.0.0.0/0", :to => "taos-demo", :type => :igw },
        ]
end
                                                  
coreo_aws_vpc_subnet "taos-demo-pub-sub" do
  action :sustain
  number_of_zones 3
  percent_of_vpc_allocated 50
  route_table "taos-demo-pub-route"
  vpc "taos-demo"
  map_public_ip_on_launch true
end
```
My friends can use that, so lets add it to github
```
$ git init .
$ git remote add origin git@github.com:CloudCoreo/t-public-network.git
$ git add . --all
$ git push -u origin master
```
Now we will create the private section
```
$ cd ../
$ mkdir taos-private
```
```
$ coreo init new-stack –stack-type stack
```
```ruby
## <repo>/services/config.rb
coreo_aws_vpc_vpc "taos-demo" do
  action :find_or_create
  cidr "33.33.0.0/16"
  internet_gateway true
end

coreo_aws_vpc_routetable "taos-demo-priv-route" do
  action :create
  vpc "taos-demo"
  number_of_tables 3
end

coreo_aws_vpc_subnet "taos-demo-priv-sub" do
  action :sustain
  number_of_zones 3
  percent_of_vpc_allocated 50
  route_table "taos-demo-priv-route"
  vpc "taos-demo"
end
```
## Slide 13
This is where what i consider to be the true power of cloudcoreo comes in. Utilizing the work done by your peers or the community

The hub is a registration for stacks that run within cloudcoreo. It is not mandatory to use it, but it makes it much nicer.

We are going to find a highly available NAT and add it to our private network stack

```
$ coreo stack add servers-nat_414ec
```
Lets take a look now at the tree

We can see that all of the files necesasry to bring up a NAT are local on the box.

One more thing to point out is the config.yaml file.  This is how you are able to clone stacks between cloud accounts and provide different settings. We do need to change a few things in this:

1. VPC_NAME
1. PUBLIC_SUBNET_NAME
1. PRIVATE_SUBNET_NAME
1. PRIVATE_SUBNETS

What if we don’t like something about this setup? We don’t own that repository! CloudCoreo allows overrides of any resource or file. Lets change our autoscaling group.
```ruby
## <repo>/services/config.rb
coreo_aws_ec2_autoscaling "${NAT_NAME}" do
  action :sustain 
  minimum 1
  maximum 1
  server_definition "${NAT_NAME}"
  subnet "taos-demo-pub-sub"
  upgrade({
            :replace => "in-place",
            :upgrade_on => "dirty",
            :cooldown => 3600
        })
end
```

Great – so now we have "taken control" of that resource, even thouse we don’t own that repostiroy. CloudCoreo allows only one instance of a resource globally in a stack, so if you name something the same, we will override it and use it as an anchor.

Awesome – this is a great stack for my friends too! Lets put it in git for later.

```
$ git init .
$ git add . --all
$ git commit -m "adding a private network with a highly agailable NAT"
$ git remote add origin git@github.com:CloudCoreo/t-private-network.git
$git push -u origin master
```

You’re done – you go home – you go to bed. Your colleagues heard that you created some awesome networking in AWS though, so you send them the git repo url for the public and the private stacks… here is what they can do with them.

## Slide 14
So lets pretend we are a new developer. Keep in mind once again, the unit of package is a git repository.

Lets start from "scratch" but use the work our colleague just created – that is called extension. Extending the "object" allows you to modify and add to the object, yet still benefit from the update that happen upstream. Lets extend the public network our colleague created

```
$ mkdir –p /var/tmp/taos-network
$ cd !$
```
```
$ coreo init new-stack --stack-type stack
$ coreo stack extend -g git@github.com:CloudCoreo/t-public-network.git
$ coreo stack add -g git@github.com:CloudCoreo/t-private-network.git -n private-network --stack-type stack
```

## Slide 15
The web ui is where you can register your repostiry as a stack and create clones based on git sha’s and/or branches. This means that deployments of infrastructure can follow the standard practice of git merging.

The UI is also where you can set CloudCoreo up to actively manage the entire stack and revert any manual changes. This forces all changes to be revisioned and tracked via git checkins.

Lets delete everything out of our cloudcoreo test account and add it here instead.

Now that we have added a stack, lets deploy a version of it.

Something to notice here is that all of the variables in the config.yaml file show up in the parameters section of the version details. This is how you can create clones quickly and easily.

The last thing I wanted to show you is what happens when an intern makes a mistake and deletes a port from the security group.


## Slide 16
First, something our customers are very excited about, is the ability to control reserved instances automatically.
Reserved instances done correctly can save about 50% on an AWS bill… the problem is actually doing them correctly.

In February, we are releasing a feature in which CloudCoreo analyzes their bill and will purchase the correct reserved instances. The purchase can happen once or can be sheduled every month, quarter, year etc. However the purchase happens, CloudCoreo will then every 3 hours check what is running vs. what reserved instances they own, and we trade them in when there is a better way to actually save money.

CloudCoreo translates the entire plan to API calls and executes them against the selected Cloud provider.

As such, we can provide true cloud portability. We will be releasing support for Azure in the beginning of Q2 this year.

